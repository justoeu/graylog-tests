package br.com.justoeu.logs.utils;

import org.springframework.util.StringUtils;

public class LoggerFormatter {

    public static String cleanup(final String json) {

        return StringUtils.isEmpty(json) ? json.replaceAll("\"", "\'") : "";

    }
}