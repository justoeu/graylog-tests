package br.com.justoeu.logs.gateway.http;

public interface URLMapping {

    String ROOT = "/api";
    String LOG_GENERATE = "/log/success/{value}";
    String CALCULATE_TAX = "/tax//calculate/{number}";
}
