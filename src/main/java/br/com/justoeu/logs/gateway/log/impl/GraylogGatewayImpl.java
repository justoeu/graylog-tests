package br.com.justoeu.logs.gateway.log.impl;

import br.com.justoeu.logs.domain.GelfMessage;
import br.com.justoeu.logs.gateway.log.GraylogGateway;
import com.google.common.collect.Lists;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@Component
public class GraylogGatewayImpl implements GraylogGateway {

    @Value("${app.graylog.username}")
    private String graylogUsername;

    @Value("${app.graylog.password}")
    private String graylogPassword;

    @Value("${app.graylog.gelf.http.port:12201}")
    private int graylogGelfHttpPort;

    @Value("${app.graylog.gelf.http.auth:false}")
    private boolean auth;

    private final RestTemplate restTemplate;
    private final UriComponentsBuilder uriBuilder;

    @Autowired
    public GraylogGatewayImpl(RestTemplate restTemplate){
        this.restTemplate = restTemplate;

        uriBuilder = UriComponentsBuilder.newInstance()
                                         .scheme(HTTP)
                                         .host(LOCALHOST)
                                         .port(graylogGelfHttpPort);
    }

    /**
     * Log message via Graylog HTTP Input (Dont forget to create this input)
      */
    @Override
    public void logEvent(GelfMessage message) {
        HttpEntity<GelfMessage> entity = new HttpEntity<>(message, buildHeaders());
        restTemplate.postForEntity(uriBuilder.cloneBuilder().port(graylogGelfHttpPort).path(PATH_GELF).toUriString(), entity, null);
    }

    @Override
    public void sendMessage(String message, String value, Map<String, Object> additionalProperties){
        GelfMessage gelfMessage = GelfMessage.builder()
                .shortMessage(message)
                .fullMessage(message)
                .host()
                .timestand()
                .additionalProperties(additionalProperties)
                .build();

        logEvent(gelfMessage);
    }

    private HttpHeaders buildHeaders() {

        HttpHeaders headers = new HttpHeaders();

        if (auth) {
            headerAuth(headers);
        }

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Lists.newArrayList(MediaType.APPLICATION_JSON));

        return headers;
    }

    private void headerAuth(HttpHeaders headers) {

        byte[] encodedAuth = Base64.encodeBase64(new StringBuilder().append(graylogUsername)
                                                                    .append(":")
                                                                    .append(graylogPassword)
                                                                    .toString()
                                                                    .getBytes(StandardCharsets.US_ASCII) );

        final String authHeader = "Basic " + new String( encodedAuth );
        headers.add(HttpHeaders.AUTHORIZATION, authHeader );
    }
}