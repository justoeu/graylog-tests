package br.com.justoeu.logs.gateway.http;

import br.com.justoeu.logs.gateway.log.GraylogGateway;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import com.google.common.base.Throwables;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping(URLMapping.ROOT)
@Api(value = "Api to Test GrayLog Connections", tags = "Logs")
@Slf4j
public class GrayLogController {

    @Autowired
    protected ObjectMapper mapper;

    @Autowired
    protected GraylogGateway gateway;

    @RequestMapping(method = RequestMethod.GET, value = URLMapping.LOG_GENERATE)
    @ApiOperation(value = "Generate Log Success")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Log Generated")
    })
    public ResponseEntity logSuccess(@PathVariable("value") String value) {
        Stopwatch timer = Stopwatch.createStarted();

        // Log message via Graylog UDP Logback Appender
        log.info("Generated log - {}", value);

        Map<String, Object> addInfos = new HashMap<>();
        addInfos.put("elapsed_time", timer.stop().elapsed(TimeUnit.MICROSECONDS));
        sendMessage(value, addInfos);

        return ResponseEntity.accepted().body("Log Generated " + value);
    }


    @RequestMapping(method = RequestMethod.GET, value = URLMapping.CALCULATE_TAX)
    @ApiOperation(value = "Generate Log Error")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "PrintStackTrace")
    })
    public ResponseEntity logFail(@PathVariable("number") long number) {

        final Stopwatch timer = Stopwatch.createStarted();

        ResponseEntity entity;
        long result;

        try {

            result = (100 / number);
            entity = ResponseEntity.ok("Result - " + result);

            // Log message via Graylog UDP Logback Appender
            log.info("Calculate Tax - result {}", result);

        } catch (ArithmeticException e){

            // Log message via Graylog UDP Logback Appender
            MDC.put("messageError", e.getMessage());
            log.info("Calculate Tax - number={}", number);

            Map<String, Object> addInfos = new HashMap<>();
            addInfos.put("elapsed_time", timer.stop().elapsed(TimeUnit.MICROSECONDS));
            addInfos.put("stackTrace", Throwables.getStackTraceAsString(e));
            sendMessage(e.getMessage(), addInfos);

            entity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

            MDC.clear();
        }

        return entity;
    }


    private void sendMessage(String value, Map<String, Object> addInfos){
        gateway.sendMessage(String.format("Log Generated via http %s", value), value, addInfos);
    }

}
