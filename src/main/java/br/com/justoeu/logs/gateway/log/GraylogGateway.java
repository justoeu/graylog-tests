package br.com.justoeu.logs.gateway.log;

import br.com.justoeu.logs.domain.GelfMessage;

import java.util.Map;

public interface GraylogGateway {

    String HTTP = "http";
    String LOCALHOST = "localhost";
    String PATH_GELF = "gelf";

    void logEvent(GelfMessage message);
    void sendMessage(String message, String value, Map<String, Object> additionalProperties);
}
