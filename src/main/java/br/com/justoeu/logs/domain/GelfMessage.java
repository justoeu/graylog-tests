package br.com.justoeu.logs.domain;

import br.com.justoeu.logs.gateway.log.GraylogGateway;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.util.ObjectUtils;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@Builder
public class GelfMessage {

    public static final String PREFIX = "_";

    @NotNull
    private String host;

    @NotNull
    @JsonProperty("short_message")
    private String shortMessage;

    @JsonProperty("full_message")
    private String fullMessage;

    @Min(1)
    private Integer level = 1;

    @JsonIgnore
    private Map<String, Object> additionalProperties;

    private Double timestamp;

    @JsonAnyGetter
    public Map<String, Object> getGelfAdditionalProperties() {
        if (additionalProperties == null) {
            return Collections.emptyMap();
        }

        return additionalProperties.entrySet().stream().collect(
                Collectors.toMap(
                    entry -> {
                        if (entry.getKey().startsWith(PREFIX)) {
                            return entry.getKey();
                        }

                        return PREFIX + entry.getKey();
                    },
                    Map.Entry::getValue)
                );
    }

    /**
     * Reimplement lombok builder to facilitate get Host and set Timestand
     */
    public static class GelfMessageBuilder {
        public GelfMessageBuilder host() {
            try {
                this.host = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) {
                this.host = GraylogGateway.LOCALHOST;
            }

            return this;
        }

        public GelfMessageBuilder timestand() {
            final Long time = System.currentTimeMillis();

            final Long seconds = TimeUnit.MILLISECONDS.toSeconds(time);
            final Double millis = (time - seconds * 1000) / 1000.0;

            this.timestamp = seconds + millis;

            return this;

        }

        public GelfMessageBuilder additionalProperties(String key, Object value) {

            additionalProperties = Optional.ofNullable(additionalProperties).orElse(new HashMap<>(0));
            additionalProperties.put(key, value);

            return this;

        }

        public GelfMessageBuilder additionalProperties(Map<String, Object> additionalProperties) {

            if (ObjectUtils.isEmpty(this.additionalProperties)){
                this.additionalProperties = additionalProperties;
            } else{
                this.additionalProperties.putAll(additionalProperties);
            }

            return this;

        }

    }

}