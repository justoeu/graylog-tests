package br.com.justoeu.logs.config;

import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

    @Value("${app.rest.max.conn:600}")
    private int maxConn;

    @Value("${app.rest.max.conn.per.route:200}")
    private int maxConnPerRoute;

    @Value("${app.rest.timeout:30000}")
    private int timeout;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(getClientHttpRequestFactory());
    }

    private HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create()
                .setMaxConnTotal(maxConn)
                .setMaxConnPerRoute(maxConnPerRoute)
                .build());

        httpComponentsClientHttpRequestFactory.setConnectTimeout(timeout);
        httpComponentsClientHttpRequestFactory.setReadTimeout(timeout);

        return httpComponentsClientHttpRequestFactory;
    }

}