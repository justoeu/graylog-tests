package br.com.justoeu.logs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraylogTestsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GraylogTestsApplication.class, args);
    }
}
